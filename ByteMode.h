//
// Created by wensi on 2023/9/21.
//

#ifndef BINARYVIEW_BYTEMODE_H
#define BINARYVIEW_BYTEMODE_H

#include <QAbstractTableModel>
#include <QFont>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <bitset>

class ByteMode: public QAbstractTableModel {

public:
    ByteMode(std::vector<std::string> tmp);
    void update(unsigned char * tmp, int size, int mode);
    void clearAndSetData();
    void switchMode(int mode);
    void getData(QList<qreal> &sum);
    int getHeight();

private:
    std::vector<unsigned char> srcData;
    std::vector<std::string> dstData;

protected:
    int rowCount(const QModelIndex &parent) const override;

    int columnCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};


#endif //BINARYVIEW_BYTEMODE_H
