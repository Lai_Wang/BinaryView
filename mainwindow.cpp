//
// Created by wensi on 2023/9/21.
//

// You may need to build the project (run Qt uic code generator) to get "ui_MainWindow.h" resolved

#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent), mode(2), isEnd(false){
    this->resize(1200,800);
    initUI();
    initMenu();
    ConnectSlots();
}

MainWindow::~MainWindow() {
    fclose(fp);
}

void MainWindow::initUI() {

    binaryView = new QTableView();
    charView = new QTableView();

    byteMode = new ByteMode(std::vector<std::string>());
    charMode = new ByteMode(std::vector<std::string>());

    binaryView->setModel(byteMode);
    charView->setModel(charMode);

    binaryView->horizontalHeader()->setVisible(false);
    binaryView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    charView->horizontalHeader()->setVisible(false);
    charView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    centerPanel.addWidget(binaryView);
    centerPanel.addWidget(charView);
    centerPanel.setStretchFactor(binaryView, 46);
    centerPanel.setStretchFactor(charView, 20);
    centerWidget.setLayout(&centerPanel);

    this->setCentralWidget(&centerWidget);
}

void MainWindow::open(std::string filename) {
    fp = fopen(filename.data(),"rb");
    if(fp == nullptr){
        std::cout<<"file open fail!"<<std::endl;
    }
    buf = (unsigned char *)malloc(BUFFER_SIZE);

    readUtils = new ReadUtils(fp, buf, BUFFER_SIZE);
    connect(readUtils,&ReadUtils::sigBuffer, this, &MainWindow::updateData);
    readUtils->start();
}

void MainWindow::updateData(unsigned char * buf, int size) {
    if(size == 0){
        readUtils->quit();
        isEnd = true;
        fclose(fp);
    }
    byteMode->update(buf,size,2);
    charMode->update(buf,size,0);
    readUtils->quit();
}

void MainWindow::ConnectSlots() {
    connect(binaryView->verticalScrollBar(), &QScrollBar::valueChanged, this, &MainWindow::synDoubleTable);
}

void MainWindow::synDoubleTable() {
    int height = binaryView->verticalScrollBar()->value();
    charView->verticalScrollBar()->setValue(height);
    std::cout<<height<<", "<<byteMode->getHeight()<<std::endl;
    if(height > byteMode->getHeight()-200 && !isEnd){
        readUtils->setFile(fp, buf,BUFFER_SIZE);
        readUtils->start();
    }
}

void MainWindow::initMenu() {
    menuBar = new QMenuBar();

    openMenu = new QMenu("文件");
    modeMenu = new QMenu("模式");
    histogramMenu = new QMenu("视图");

    QAction *fileAction = new QAction("打开");
    openMenu->addAction(fileAction);

    QAction *binMode = new QAction("二进制");
    QAction *octMode = new QAction("八进制");
    QAction *decMode = new QAction("十进制");
    QAction *hexMode = new QAction("十六进制");

    modeMenu->addAction(binMode);
    modeMenu->addAction(octMode);
    modeMenu->addAction(decMode);
    modeMenu->addAction(hexMode);

    QAction *chartAction = new QAction("直方图");
    histogramMenu->addAction(chartAction);

    connect(fileAction, &QAction::triggered,this,&MainWindow::openFile);
    connect(chartAction, &QAction::triggered,this,&MainWindow::displayHistogram);
    connect(binMode, &QAction::triggered,[=](){mode = 2;byteMode->switchMode(mode);});
    connect(octMode, &QAction::triggered,[=](){mode = 8;byteMode->switchMode(mode);});
    connect(decMode, &QAction::triggered,[=](){mode = 10;byteMode->switchMode(mode);});
    connect(hexMode, &QAction::triggered,[=](){mode = 16;byteMode->switchMode(mode);});

    menuBar->addMenu(openMenu);
    menuBar->addMenu(modeMenu);
    menuBar->addMenu(histogramMenu);

    menuBar->setStyleSheet("font-size: 16px;");

    this->setMenuBar(menuBar);
    statusBar = new QStatusBar();
    this->setStatusBar(statusBar);
}

void MainWindow::openFile() {
    filename = QFileDialog::getOpenFileName(this, "打开文件",QDir::homePath());
    if(!filename.isEmpty()){
        byteMode->clearAndSetData();
        charMode->clearAndSetData();
        open(filename.toStdString());
        statusBar->showMessage(filename);
    }
}

void MainWindow::displayHistogram() {
    QChart *charPanel = new QChart();
    QBarSet *xSet = new QBarSet("数值");
    QBarSeries *barSeries = new QBarSeries();

    QList<qreal> sum(256,0);
    byteMode->getData(sum);
    xSet->append(sum);
    barSeries->append(xSet);

    int max_q = 0;
    for(auto q:sum){
        if(q>max_q)
            max_q = q;
    }

    QValueAxis *Xaxis = new QValueAxis();
    Xaxis->setRange(0,256);
    Xaxis->setLabelsAngle(45);
    Xaxis->setLabelFormat("%d");
    Xaxis->setTickCount(16);

    QValueAxis *Yaxis = new QValueAxis();
    Yaxis->setRange(0,max_q);
    Yaxis->setLabelFormat("%d");
    Yaxis->setTickCount(8);

    charPanel->addSeries(barSeries);
    charPanel->addAxis(Xaxis, Qt::AlignBottom);
    charPanel->addAxis(Yaxis,Qt::AlignLeft);

    QChartView * chartView = new QChartView();
    chartView->setChart(charPanel);
    chartView->setRenderHint(QPainter::Antialiasing, true);
    charView->setGeometry(this->x(), this->y(), 1200, 800);
    chartView->show();
}

