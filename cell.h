//
// Created by wensi on 2023/9/21.
//

#ifndef BINARYVIEW_CELL_H
#define BINARYVIEW_CELL_H

#include <QWidget>

class Cell : public QWidget {
Q_OBJECT

public:
    explicit Cell(QWidget *parent = nullptr);

    ~Cell() override;

private:
protected:
    void paintEvent(QPaintEvent *event) override;
};


#endif //BINARYVIEW_CELL_H
