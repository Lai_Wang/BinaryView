//
// Created by wensi on 2023/9/21.
//

#include "ReadUtils.h"

void ReadUtils::run() {
    size_t len = fread(buf, sizeof(unsigned char),size,sfp);
    sigBuffer(buf, len);
}

ReadUtils::ReadUtils(FILE *fp, unsigned char * buf, int size) {
    this->sfp = fp;
    this->buf = buf;
    this->size = size;
}

void ReadUtils::setFile(FILE *fp, unsigned char *buf, int size) {
    this->sfp = fp;
    this->buf = buf;
    this->size = size;
}
