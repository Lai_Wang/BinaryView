//
// Created by wensi on 2023/9/21.
//

#ifndef BINARYVIEW_READUTILS_H
#define BINARYVIEW_READUTILS_H

#include <QThread>
#include <QString>
#include <vector>
#include <iostream>


class ReadUtils: public QThread{
    Q_OBJECT
public:
    ReadUtils(FILE *fp, unsigned char * buf, int size);
//    ~ReadUtils();
    void run() override;
    void setFile(FILE *fp, unsigned char * buf, int size);

signals:
//    void sigBuffer(std::vector<unsigned char> buf, int size);
    void sigBuffer(unsigned char * buf, int size);
private:
    FILE *sfp;
    unsigned char * buf;
    int size;
};


#endif //BINARYVIEW_READUTILS_H
