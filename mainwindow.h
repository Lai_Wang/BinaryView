//
// Created by wensi on 2023/9/21.
//

#ifndef BINARYVIEW_MAINWINDOW_H
#define BINARYVIEW_MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QTableView>
#include <QFile>
#include <cstdio>
#include <QDataStream>
#include <vector>
#include <QHeaderView>
#include <QDebug>
#include <iostream>
#include <QScrollBar>
#include <QMenuBar>
#include <QMenu>
#include <QFileDialog>
#include <QAction>
#include <QStatusBar>
#include <QList>
#include <QtCharts/QChart>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QBarCategoryAxis>

#include "ByteMode.h"
#include "ReadUtils.h"

#define BUFFER_SIZE  1024

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    void open(std::string filename);
    ~MainWindow();

private:
    void initUI();
    void ConnectSlots();
    void synDoubleTable();
    void initMenu();

public Q_SLOTS:
    void updateData(unsigned char * buf, int size);
    void openFile();
    void displayHistogram();

private:
    bool isEnd;
    int mode;
    QString filename;
    unsigned char * buffer;
    QTableView *binaryView;
    QTableView *charView;
    unsigned char * buf;
    FILE *fp;
    ByteMode *byteMode;
    ByteMode *charMode;
    ReadUtils *readUtils;

    QWidget centerWidget;
    QHBoxLayout centerPanel;

    QMenuBar *menuBar;
    QMenu *openMenu;
    QMenu *modeMenu;
    QMenu *histogramMenu;

    QStatusBar *statusBar;
};


#endif //BINARYVIEW_MAINWINDOW_H
