//
// Created by wensi on 2023/9/21.
//

#include "ByteMode.h"

int ByteMode::rowCount(const QModelIndex &parent) const {
//    return dstData.size() / 16;
    if(dstData.size() == 0)
        return dstData.size() / 16;
    else{
        return static_cast<int>((dstData.size() / 16 ) + 1);
    };
}

int ByteMode::columnCount(const QModelIndex &parent) const {
    return 16;
}

QVariant ByteMode::data(const QModelIndex &index, int role) const {
    if(role == Qt::DisplayRole){
        int si = index.row()*16 + index.column();
        std::string str = "";
        if(si < dstData.size())
            str = dstData[index.row()*16 + index.column()];
        return QString(str.c_str());
    }else if(role == Qt::TextAlignmentRole){
        return Qt::AlignCenter;
    }else if(role == Qt::FontRole){
        return QFont("YaHei",10);
    }
    return QVariant();
}

QVariant ByteMode::headerData(int section, Qt::Orientation orientation, int role) const {
    return QAbstractItemModel::headerData(section, orientation, role);
}

void ByteMode::update(unsigned char * tmp, int size, int mode) {
    beginResetModel();
    if(srcData.capacity() == 0){
        srcData.reserve(size);
        dstData.reserve(size);
    }
    if(srcData.capacity() - srcData.size() < size){
        srcData.reserve(static_cast<int>(1.5 * srcData.capacity()));
        dstData.reserve(static_cast<int>(1.5 * srcData.capacity()));
    }
    int old_size = srcData.size();
    srcData.resize(srcData.size() + size);
    for (int i = old_size; i < srcData.size(); ++i) {
        srcData[i] = tmp[i];
    }
    switchMode(mode);
    endResetModel();
}

ByteMode::ByteMode(std::vector<std::string> tmp) {
    dstData.swap(tmp);
}

void ByteMode::clearAndSetData() {
    this->srcData.resize(0);
    this->srcData.shrink_to_fit();
    this->dstData.resize(0);
    this->dstData.shrink_to_fit();
}

void ByteMode::switchMode(int mode) {
    beginResetModel();
    if (dstData.capacity() < srcData.capacity()){
       dstData.reserve(srcData.capacity());
    }
    if (dstData.size() < srcData.size()){
        dstData.resize(srcData.size());
    }

    std::string s = "";
    for(int i = 0; i < dstData.size(); ++i){
        switch (mode) {
            case 0:{
                if(srcData[i]>32 && srcData[i]<127){
                    s = char(srcData[i]);
                }
                break;
            }
            case 2:{
                std::bitset<8> binaryNumber(srcData[i]);
                s = binaryNumber.to_string();
                break;
            }
            case 8:{
                std::ostringstream oss;
                oss<<std::setbase(8)<<int(srcData[i]);
                s = oss.str();
                break;
            }
            case 10:{
                s = std::to_string(srcData[i]);
                break;
            }
            case 16:{
                std::ostringstream oss;
                oss<<std::hex<<int(srcData[i]);
                s = "0x" + oss.str();
                break;
            }

        }
        dstData[i] = s;
    }
    endResetModel();
}

void ByteMode::getData(QList<qreal> &sum) {
    for(auto d:srcData){
        ++sum[d];
    }
}

int ByteMode::getHeight() {
    return srcData.size() / 16;
}
